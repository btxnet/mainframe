/*
 * Copyright (c) 2018.
 *
 * -> Twitter: @Variiuz
 *
 * // Coded by Variiuz / VariusDev for Beteax.net
 */

package net.beteax.channels;

import net.beteax.framework.UUIDFetcher;
import net.beteax.framework.auth.Base64Auth;
import net.beteax.resources.Zeiteinheiten;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.*;

public class LobbyChannel implements Listener {
    private String basecode = new Base64Auth("LobbyPluginCode4").e();
    private byte[] data;
    private boolean allowed = false;
    private boolean done = false;
    private ServerInfo info;
    private String msg = null;

    public LobbyChannel(byte[] data, ServerInfo sender){
        this.data = data;
        this.info = sender;
    }
    public LobbyChannel confirm(){
        if(!done) {
            ByteArrayInputStream r = new ByteArrayInputStream(this.data);
            DataInputStream n = new DataInputStream(r);

            try {
                String[] s = n.readUTF().split(";");
                //Compare base64 Code
                if (s[0].equals(this.basecode)) allowed = true;
                else allowed = false;
                done = true;
            } catch (IOException ignored) {
            }
        }
        return this;
    }
    public void handle(){
        confirm();
        if(allowed){
            ByteArrayInputStream r = new ByteArrayInputStream(this.data);
            DataInputStream n = new DataInputStream(r);

            try {
                String[] s = n.readUTF().split(";");
                String f = s[1];//Value 1
                String h = s[2];//Value 2
                //String x = s[3];
                if(f != null || h != null) {
                    if (f.equals("REQUESTINFO")) {//Only Request right now. (Returns Userinfo.)
                        if (h.length() <= 16){//Username cuz <= 16 chars
                            if(UUIDFetcher.isInDatabase(h)){

                            }else msg = "NO_USER_IN_DATABASE";
                        }else {//UUID bc it's longer than 16 chars
                            if(UUIDFetcher.isInDatabaseUUID(h)){

                            }else msg = "NO_UUID_IN_DATABASE";
                        }
                    } else allowed = false;
                }else allowed = false;
                if(allowed && msg != null)sendMessage();
            } catch (IOException ignored) {
            }
        }
    }
    public void sendMessage() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF(this.msg);
        } catch (IOException e) {
            e.printStackTrace();
        }

        info.sendData("Mainframe", stream.toByteArray());
    }
    public static void updateClan(String clanname, ServerInfo info){
        String m = "UPDATE;CLAN;"+clanname+";";
        sendMessage(m,info);
    }
    public static void reloadGroups(String player, ServerInfo playerserver){
        String m = "UPDATE;GROUP;"+player+";";
        sendMessage(m,playerserver);
    }
    public static void requestLobbyShutdown(ServerInfo i){
        String m = "REQUEST;SERVER;SHUTDOWN;"+new Base64Auth("CodeGamma").e()+";";
        sendMessage(m,i);
    }
    private static void sendMessage(String message, ServerInfo server) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.sendData("Mainframe", stream.toByteArray());
    }
}
